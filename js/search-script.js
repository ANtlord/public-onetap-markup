function SearchCtrl($scope) {
    $scope.sections = data.sections;
}

var app = angular.module('searchApp', ['uiSlider']);

app.directive('ngTabs', function(){
    /*
     * Directive for extending directive ngRepeat (ng-repeat), which
     * highlight element by attribute is_active.
     */
    return {
        link: function(scope, elem, attrs, controller){
            if (scope.item['is_active'] == true) {
                elem.addClass('active');
            }
        }
    }
});
