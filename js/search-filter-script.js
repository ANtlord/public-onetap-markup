function SearchFilterCtrl($scope){
    $scope.industries = data.industries;
    $scope.lower_price_bound = 0;
    $scope.upper_price_bound = 50;
    $scope.min = 0;
    $scope.max = 1000;
    $scope.step = 10;

    $scope.priceRange = function(item) {
        return (parseInt(item['min-acceptable-price']) >= $scope.lower_price_bound && parseInt(item['max-acceptable-price']) <= $scope.upper_price_bound);
    };

}
