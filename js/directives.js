angular.module('OnetapApp.directives', []).
    directive('ngTabs', function(){
        /*
         * Directive for extending directive ngRepeat (ng-repeat), which
         * highlight element by attribute is_active.
         */
        return {
            link: function(scope, elem, attrs, controller){
                if (scope.item['is_active'] == true) {
                    elem.addClass('active');
                }
            }
        }
    }).
    directive('lazyInclude', ['$http', '$compile', function($http, $compile){
        return {
            restrict: 'E',
            link: function(scope, elem, attrs, controller){
                var url = elem.attr('src');
                $http.get(url).
                    success(function(data, status, headers, config) {
                        elem.html($compile(data)(scope));
                    }).
                    error(function(data, status, headers, config) {
                        console.log('error loading url: '+url);
                    });
            },
        }
    }]);
