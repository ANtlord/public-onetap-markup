angular.module('OnetapApp', ['ngRoute', 'ngSanitize', 'OnetapApp.directives',
    'OnetapApp.controllers', 'OnetapApp.constants'
]).
config(['$routeProvider', '$locationProvider', '$httpProvider', 'localURLConfig',
    function($routeProvider, $locationProvider, $httpProvider, localURLConfig) {
        //$locationProvider.html5Mode(true);
        $locationProvider.html5Mode(false).hashPrefix('!');
        $routeProvider.
            when('/', {templateUrl:'html/main_search.html', controller: 'MainCtrl'}).
            when(localURLConfig.INDUSTRY_LIST, {
                templateUrl:'html/industries.html', controller: 'ItemsCtrl'
            }).
            when(localURLConfig.COMPANY_LIST+':id/', {
                templateUrl:'html/companies.html', controller: 'ItemsCtrl'
            }).
            when(localURLConfig.CATEGORY_LIST+':id/', {
                templateUrl:'html/categories.html', controller: 'ItemsCtrl'
            }).
            when(localURLConfig.PRODUCT_LIST+':id/', {
                templateUrl:'html/products.html', controller: 'ItemsCtrl'
            }).
            when(localURLConfig.PRODUCT_DETAIL+':id/', {
                templateUrl:'html/product.html', controller: 'ItemCtrl'
            }).
            when(localURLConfig.SEARCH, {
                templateUrl:'html/tabview.html', controller: 'SearchCtrl'
            }).
            when(localURLConfig.FAVORITES, {
                templateUrl:'html/tabview.html', controller: 'FavoritesCtrl'
            }).
            otherwise({redirectTo: '/'});
    }
]);
