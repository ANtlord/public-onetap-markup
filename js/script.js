function MenuCtrl($scope) {
    $scope.menuItems = data.menuItems;
}

function BottomMenuCtrl($scope) {
    $scope.bottomMenuItems = data.bottomMenuItems;
}

function HeaderCtrl($scope) {
    $scope.searchIsShow = false;
}

$(document).ready(function(){
    $('.left-base').on({
        mouseenter: function(){
            $(this).css('background', $(this).data('color'));
        },
        mouseleave: function() {
            $(this).css('background', 'none');
        }
    },'.left-menu_item');
    $('.content-header_dropdown-toggle').dropdown()
});
