var items = 
angular.module('OnetapApp.controllers', [
    'ngRoute',
    'uiSlider',
    'OnetapApp.constants',
    'OnetapApp.factories',
    'ngCookies',
    'autocomplete']).
    controller('MainCtrl', ['$scope', 'LeftMenuFactory', 'HeaderSearchFactory',
        function($scope, LeftMenuFactory, HeaderSearchFactory){
            LeftMenuFactory.cleanBreadcrumps();
            HeaderSearchFactory.searchString = "";
            HeaderSearchFactory.isHide = true;
            $scope.HeaderSearchFactory = HeaderSearchFactory;
        }
    ]).
    controller('MenuCtrl', ['$scope', '$location', 'LeftMenuFactory',
        function($scope, $location, LeftMenuFactory) {
            $scope.menuItems = LeftMenuFactory.items;
            /*
             * Method for changing background of activated item of the left menu.
             */
            function setActiveItem(url) {
                for (var i=0; i<$scope.menuItems.length; ++i){
                    if (url == $scope.menuItems[i].link ||
                       '#!'+url == $scope.menuItems[i].link) {
                        $scope.menuItems[i].is_active = true;
                    }
                    else $scope.menuItems[i].is_active = false;
                }
            }

            setActiveItem($location.$$url);

            $scope.menuClick = function(item) {
                setActiveItem(item.link);
            }

            $scope.isSelected = function(item) {
                return item.is_active;
            }
        }
    ]).
    controller('BottomMenuCtrl', ['$scope', 
        function BottomMenuCtrl($scope) {
            $scope.bottomMenuItems = data.bottomMenuItems;
        }
    ]).
    controller('ItemsCtrl', ['$scope', 'serviceURLConfig',
         'localURLConfig', '$location', 'LeftMenuFactory',
        'HeaderSearchFactory', 'HelperFactory', 'ListItemsFactory',
        function($scope,  serviceURLConfig, localURLConfig,
                 $location, LeftMenuFactory, HeaderSearchFactory,
            HelperFactory, ListItemsFactory)
        {
            HeaderSearchFactory.isHide = false;
            // Generates query to API.
            var url = (serviceURLConfig.HOST+serviceURLConfig.SERVICE_URI
                       +$location.$$path+'?withSection=1');
            // Gets details of company if downloading first page of categories.
            if ($location.$$path.indexOf(localURLConfig.CATEGORY_LIST) != -1){
                url += "&withSectionDetails=1";
            }
            // Create breadcrumps if they doesn't exits and current page
            // doesn't represent industries.
            //var menu_c1 = (('subitems' in LeftMenuFactory.items[1] == false)
                      //|| (LeftMenuFactory.items[1].subitems.length == 0));
            var menu_c2 = (localURLConfig.INDUSTRY_LIST != $location.$$path);
            LeftMenuFactory.cleanBreadcrumps();
            if (menu_c2) {
                url += '&breadcrumps=1';
            }
            ListItemsFactory.parseItems($scope, url, menu_c2);
        }
    ]).
    controller('SearchCtrl', ['$scope', 'serviceURLConfig', 'HeaderSearchFactory',
        'TypesMenuFactory', '$routeParams', 'ListItemsFactory',
        function($scope, serviceURLConfig, HeaderSearchFactory,
                TypesMenuFactory, $routeParams, ListItemsFactory)
        {
            HeaderSearchFactory.isHide = false;
            var type = 'product';
            if ('type' in $routeParams) {
                type = $routeParams['type'];
            }
            if (type == 'product') {
                angular.element('head').append(
                    '<link rel="stylesheet" href="css/products-style.css" />');
            }
            $scope.sections = TypesMenuFactory.generateItems(
               '#!/search'+'?q=' + $routeParams['q'], type, false);
            //$scope.max = 100000;
            //$scope.min = 0;    
            //$scope.step = 10;    
            //$scope.upper_price_bound = $scope.max;
            //$scope.lower_price_bound = $scope.min;
            var url = (serviceURLConfig.HOST+serviceURLConfig.SEARCH_URI
               + '?q=' + $routeParams['q'] + '&type=' + type);
            ListItemsFactory.parseItems($scope, url, false);
        }
    ]).
    controller('HeaderCtrl', ['$scope', '$timeout', 'HeaderSearchFactory',
        function( $scope, $timeout, HeaderSearchFactory) {
            $scope.HeaderSearchFactory = HeaderSearchFactory;
            var timeID = null;
            $scope.updateMovies = function(value){
                if (value.length >= 2) {
                    if (timeID != null) {
                        $timeout.cancel(timeID);
                    }
                    timeID = $timeout(function(){
                        $scope.HeaderSearchFactory.updateCompletions(value);
                        console.log(123);
                    }, 500);
                }
            }
        }
    ]).
    controller('ItemCtrl', ['$scope', '$http', 'serviceURLConfig', '$location',
        'LeftMenuFactory', 'FavoritesFactory',
        function($scope, $http, serviceURLConfig, $location, LeftMenuFactory,
                 FavoritesFactory)
        {
            LeftMenuFactory.cleanBreadcrumps();
            var url = (serviceURLConfig.HOST+serviceURLConfig.SERVICE_URI
                       +$location.$$path);
            url += '?breadcrumps=1';
            $http.get(url).
                success(function(data, status, headers, config) {
                    $scope.object = data;
                    LeftMenuFactory.setBreadcrumps(data);
                    var TYPE = data['model'].substring(
                        data['model'].indexOf('.')+1, data['model'].length);
                    FavoritesFactory.includeToggling($scope, TYPE);
                }).
                error(function(data, status, headers, config) {
            
                });
        }
    ]).
    controller('ObjectCtrl', ['$scope', function($scope){
    }]).
    controller('DetailObjectCtrl', ['$scope', function($scope){
    }]).
    controller('FavoritesCtrl', ['$scope', '$routeParams', 'TypesMenuFactory',
        'ListItemsFactory', 'serviceURLConfig', 'HeaderSearchFactory',
        "FavoritesFactory", "dictionary",
        function($scope, $routeParams, TypesMenuFactory, ListItemsFactory,
            serviceURLConfig, HeaderSearchFactory, FavoritesFactory, dictionary)
        {
            HeaderSearchFactory.isHide = false;
            var type = 'product';
            if ('type' in $routeParams) {
                type = $routeParams['type'];
            }
            if (type == 'product') {
                angular.element('head').append(
                    '<link rel="stylesheet" href="css/products-style.css" />');
            }
            var IDS = FavoritesFactory.getFavorites(type, true);
            $scope.sections = TypesMenuFactory.generateItems(
               '#!/favorites/', type, true);

            // No favorites? No problem.
            if (IDS != "[]") {
                var url = (serviceURLConfig.HOST+FavoritesFactory.getServiceURL(type)
                   + "?ids="+IDS);
                ListItemsFactory.parseItems($scope, url, false);
            }
            else {
                $scope.thumbMessage = dictionary.NO_ITEMS;
            }
        }
    ]);
