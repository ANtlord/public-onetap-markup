angular.module('OnetapApp.factories', ['OnetapApp.constants', 'ngCookies', ]).
    factory("FavoritesFactory", ['$cookies',
        function($cookies){
            return {
                addFavorite: function(id, type) {
                    this.setFavorite(id, type, true);
                },
                delFavorite: function(id, type) {
                    this.setFavorite(id, type, false);
                },
                setFavorite: function(id, type, flag){
                    var favorites = this.getFavorites(type, false);
                    this.setPassedFavorite(id, type, favorites, flag);
                },
                setPassedFavorite: function(id, type, favorites, flag) {
                    var N = favorites.indexOf(id);

                    if (N == -1 && flag == true) {  // Add to favorites.
                        favorites.push(id);
                        this.setFavorites(type, favorites);
                    }
                    else if (N != -1 && flag == false) {    // Delete from favorites.
                        favorites.pop(id);
                        this.setFavorites(type, favorites);
                    }
                },
                /*
                 *  Possible values of type "product", "category", "company".
                 *  Parameters:
                 *      type - (String) type of items, which are need.
                 *      isString - flag which says, what type of result we need.
                 *      false - returns array, true - string.
                 *  Returns array of favorites of the passed type from cookies.
                 */
                getFavorites: function(type, isString) {
                    if (("onetap-"+type+"-favorites" in $cookies) == false) {
                        // Don't use method setsetFavorites, because usage
                        // it - is additional actions.
                        $cookies["onetap-"+type+"-favorites"] = "[]";
                        if (isString) return "[]";
                        return [];
                    }
                    var cookieString = $cookies["onetap-"+type+"-favorites"];
                    var N = cookieString.lastIndexOf(";expires=");
                    if (N != -1) {
                        cookieString = cookieString.substring(0, N);
                    }
                    if (isString) return $cookies["onetap-"+type+"-favorites"]; 
                    return JSON.parse(cookieString);
                },
                /**
                 *  Method generate URL for getting items from backend. 
                 *  Returns string.
                 */
                getServiceURL: function(type){
                    return "/service/get/custom-"+type+"-list/";
                },
                /*
                 *  Sets array of values to cookies in passed type.
                 */
                setFavorites: function(type, values) {
                    var now = new Date();
                    var time = now.getTime();
                    time += 24*60*60*1000*7;
                    now.setTime(time);
                    $cookies["onetap-"+type+"-favorites"] = (JSON.stringify(
                        values) + ";expires=" + now.toGMTString());
                },
                /*
                 *  Method creates class for favorite button and include related
                 *  set of functions.
                 */
                includeToggling: function($scope, type){
                    var favorites = this.getFavorites(type, false);
                    var ID = $scope.object['id'];
                    $scope.isFavorite = favorites.indexOf(ID) != -1;
                    var self = this;
                    $scope.toggleFavorites = function(){
                        $scope.isFavorite = !$scope.isFavorite;
                        self.setPassedFavorite(ID, type, favorites,
                            $scope.isFavorite);
                    }
                }
            }
        }]
    ).
    factory('LeftMenuFactory', ['localURLConfig', 'HelperFactory',
        function(localURLConfig, HelperFactory){
            return {
                items: [
                    {link: '/', text: 'Поиск', color: '#f00', image: 'search',
                        is_active: false},
                    {link: '#!'+localURLConfig.INDUSTRY_LIST, text: 'Категории',
                        color: '#0f0', image: 'list', is_active: false},
                    {link: '#!'+localURLConfig.FAVORITES, text: 'Избранное', color: '#00f', image: 'star',
                        is_active: false}
                ],
                cleanBreadcrumps: function(){
                    if ('subitems' in this.items[1]) {
                        delete this.items[1]['subitems'];
                    }
                },
                cleanBreadcrumpsFromItem: function(idx){
                    var breadcrumps = this.items[1].subitems;
                    this.items[1].subitems = [];
                    for (var i=0; i<idx+1; ++i){
                        this.items[1].subitems.push(breadcrumps[i]);
                    }
                },
                setBreadcrumps: function(data){
                    for(var i=0; i<data['breadcrumps'].length; ++i){
                        var item = {text: data['breadcrumps'][i].text};
                        item['link'] = HelperFactory.modificateLink(data['breadcrumps'][i].link);
                        if(('subitems' in this.items[1]) == false)
                            this.items[1].subitems = [];
                        this.items[1].subitems.push(item);
                    }
                }
            }
        }]
    ).
    factory('HeaderSearchFactory', ['serviceURLConfig', '$http',
        function(serviceURLConfig, $http){
            return {
                isHide: true,
                searchString: "",
                searchSubmit: function(){
                    window.location = '#!/search/?q='+this.searchString;
                },
                completions: [],
                updateCompletions: function(value){
                    var self = this;
                    $http.get(serviceURLConfig.HOST
                              +serviceURLConfig.SEARCH_COMPLETION_URI+value).
                        success(function(data, status, headers, config) {
                            self.completions = data['results'];
                        }).
                        error(function(data, status, headers, config) {
                            self.completions = [];
                            console.log(data);
                        });
                }
            }
        }]
    ).
    factory('HelperFactory', [
        function(){
            return {
                /*
                 *  Modificate link from service to local app link.
                 */
                modificateLink: function (link) {
                    var KEY = '/get/';
                    var pos = link.indexOf(KEY);
                    return link.substring(pos+KEY.length,link.length);
                }
            }
        }]
    ).
    factory('TypesMenuFactory', ['localURLConfig',
        function(){
            return {
                items: [
                    {link: '#', text: 'Индустрии', is_active: false},
                    {link: '#', text: 'Компании', is_active: false},
                    {link: '#', text: 'Категории', is_active: false},
                    {link: '#', text: 'Товары', is_active: true}
                ],
                generateItems: function(prefix, type, isFirst){
                    var symbol = isFirst ? '?' : '&';
                    var items = [
                        {link: prefix+symbol+'type=industry', text: 'Индустрии', is_active: false},
                        {link: prefix+symbol+'type=company', text: 'Компании', is_active: false},
                        {link: prefix+symbol+'type=category', text: 'Категории', is_active: false},
                        {link: prefix+symbol+'type=product', text: 'Товары', is_active: false}
                    ];

                    for (var i=0; i<items.length; ++i){
                        if (items[i].link.indexOf(type) != -1) {
                            items[i].is_active = true;
                        }
                    }
                    return items;
                }
            }
        }]
    ).
    factory('ListItemsFactory', ['$http', 'HelperFactory', 'serviceOptions',
        'LeftMenuFactory', 'FavoritesFactory', "dictionary",
        function($http, HelperFactory, serviceOptions, LeftMenuFactory, 
            FavoritesFactory, dictionary)
        {
            return {
                /*
                 * Method for parsing items and extend incamed scope.
                 * Params:
                 * url - address where program downloads data for parsing.
                 */
                parseItems: function($scope, url, isNeedMenu){
                    $http.get(url).
                        success(function(data, status, headers, config) {
                            // Add section if it possible.
                            if ('section' in data) {
                                $scope.object = data.section;   
                                var TYPE = data.section['model'].substring(
                                    data.section['model'].indexOf('.')+1,
                                    data.section['model'].length);
                                FavoritesFactory.includeToggling($scope, TYPE);
                            }
                            // Parse results if it possible.
                            if (data['results'] != null && data.results.length > 0) {
                                $scope.items = data['results'];
                                // Create links for moving between pages in application.
                                for (var i=0; i<$scope.items.length; ++i){
                                    $scope.items[i].link =
                                        HelperFactory.modificateLink($scope.items[i].url);
                                }
                                
                                // Gets pages if they are exists.
                                if (data.next != null) {
                                    var count = data.count;
                                    var N = count / serviceOptions.PAGINATE_BY;
                                    $scope.pages = [];
                                    for (var i=0; i<N; ++i){
                                        $scope.pages.push(
                                            {link: '?page='+i, text: i, isCurrent: false}
                                        );
                                    }
                                }
                            }
                            else {
                                $scope.thumbMessage = dictionary.NO_ITEMS;
                            }
         
                            // Parse breadcrumps.
                            if (isNeedMenu == true) {
                                LeftMenuFactory.setBreadcrumps(data);
                            }
                        }).
                        error(function(data, status, headers, config) {
                            alert("Возникла ошибка");
                    });
                }
            }
        }]
    );
    
