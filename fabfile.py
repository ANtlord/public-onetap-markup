# -*- coding: utf-8 -*-
from fabric.api import run
from fabric.api import cd
from fabric.api import env
from jsmin import jsmin
from fabric.api import execute
from rcssmin import cssmin
import getpass
from fabric.api import settings
from fabric.api import hosts
from fabric.api import sudo
from fabric.api import roles
from fabric.api import prefix
from fabric.api import local
from fabric.contrib import django
import sys
import os

BASE_DIR = os.path.dirname(__file__)
env.use_ssh_config = True
env.hosts = ['onetap@one-tap.ru']
#env.path = "/home/uantlord/Develop/onetap-django/.venv/bin/activate"

root_dir = '/www/public-onetap-markup'
PRODUCTION = {
        'root_dir': root_dir,
}

def update():
    """Method for updating code"""

    # Update code.
    local("git push origin master")
    with cd(PRODUCTION['root_dir']):
        run("git pull origin master")

def build():
    path = os.path.join(BASE_DIR, 'build')
    if not os.path.exists(path): os.mkdir(path) 
    def minify(source_path, file_name):
        files = os.listdir(source_path)
        f = open(os.path.join(path, file_name), 'w')
        f.write('')
        for item in files:
            if item.find('.min') == -1 and item.find('products-style') == -1:
                with open(os.path.join(source_path, item)) as source_file:
                    minified = None
                    if file_name[len(file_name)-3:] == 'css': minified = cssmin(source_file.read(), keep_bang_comments=True)
                    else: minified = jsmin(source_file.read())

                    f.write(minified)
        f.close()

    scripts_path = os.path.join(BASE_DIR, 'js')
    minify(scripts_path, 'script.min.js')
    scripts_path = os.path.join(BASE_DIR, 'css')
    minify(scripts_path, 'style.min.css')




def deploy():
    update()
    backup()
    install_deps()
    collect()
    migrate()
    restart()
